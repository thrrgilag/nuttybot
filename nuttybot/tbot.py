import telebot
import pnutpy
import logging
import argparse
import threading
import redis
import json
import time
import os
import re

from configobj import ConfigObj
from nuttybot.models import *

REDIS_HOST = os.environ.get('REDIS_HOST', 'localhost')
REDIS_PORT = os.environ.get('REDIS_PORT', 6379)
NUTTYBOT_CONFIG = os.environ.get("NUTTYBOT_CONFIG")
NUTTYBOT_STORE = os.environ.get("NUTTYBOT_STORE")

rs = redis.Redis(host=REDIS_HOST, port=REDIS_PORT)

_shutdown = threading.Event()

pnut_auth_url = "https://pnut.io/oauth/authenticate"
pnut_redirect = "urn:ietf:wg:oauth:2.0:oob"
pnut_scopes = "basic,stream,write_post,follow,presence,messages,files"

bot_commands = {
    'start': "Display the welcome messaage",
    'connect <token>': "Store auth token provided after authorizing NuttyBot",
    'disconnect': "Remove auth token and disable NuttyBot",
    'enable': "Enable Telegram notifications in NuttyBot",
    'disable': "Disable Telegram notifications in NuttyBot",
    'copy_on': "Enable notification on copy mention (/@username)",
    'copy_off': "Disable notification on copy mention (/@username)",
    'pm_on': "Enable private message (pm) notifications",
    'pm_off': "Disable private message (pm) notifications",
    'settings': "Display your NuttyBot notification settings",
    'post <text>': "Post new message to your timeline",
    'reply <postid> <text>': "Reply to the poster of postid",
    'replyall <postid> <text>': "Reply to all mentioned in postid",
    'replyg <postid> <text>': "Reply to postid with a global context (no @)",
    'replyh <postid> <text>': "Reply to all mentioned but add others at end with a /",
    'ping': "Check if NuttyBot is alive",
    'help': "Display this help text"
}

def mqueue(bot):

    while not _shutdown.isSet():
        try:
            item = rs.lpop('TG')
            if item is not None:
                msg = json.loads(item)
                logging.debug(msg)
                bot.send_message(msg['user'], msg['message'])
            time.sleep(2)

        except redis.exceptions.ConnectionError:
            logging.warning("--- redis connection error ---")
            logging.warning("sleeping for 30...")
            time.sleep(30)

        except Exception:
            logging.exception("mqueue?")
            logging.error("--- unknown redis error ---")
            logging.warning("sleeping for 30...")
            time.sleep(30)

    logging.info("exiting mqueue")

def main():
    a_parser = argparse.ArgumentParser()
    a_parser.add_argument(
        '-d', action='store_true', dest='debug',
        help="debug logging"
    )
    a_parser.add_argument(
        '-c', '--config', default="config.ini",
        help="configuration file"
    )
    a_parser.add_argument(
        '-s', '--store', default="store.db",
        help="sqlite database filepath"
    )
    args = a_parser.parse_args()

    if args.debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    if NUTTYBOT_CONFIG is None:
        filename = args.config
    else:
        filename = NUTTYBOT_CONFIG

    if NUTTYBOT_STORE is None:
        db_file = args.store
    else:
        db_file = NUTTYBOT_STORE

    config = ConfigObj(filename)

    if "pnut" not in config:
        logging.error("pnut section missing from configuration")
        exit(1)
    elif "client_id" not in config["pnut"]:
        logging.error("client_id missing from pnut section in configuration")
        exit(1)

    if "telegram" not in config:
        logging.error("telegram section missing from configuration")
        exit(1)
    elif "bot_token" not in config["telegram"]:
        logging.error("bot_token missing from telegram section in configuration")
        exit(1)

    db.init(db_file)
    create_tables()

    bot = telebot.TeleBot(config["telegram"]["bot_token"], parse_mode=None)

    def pnut_user_api(tg_uid):
        try:
            user = User.get(User.tg_user == tg_uid)
            if user.pnut_token and len(user.pnut_token) > 0:
                return pnutpy.API.build_api(access_token=user.pnut_token)
            else:
                return None

        except User.DoesNotExist:
            return None

        except Exception:
            logging.exception("pnut_user_api")
            return None

    def pnut_post(api, text):
        try:
            post, meta = api.create_post(data={'text': text})
            return f"* created post {post['id']}"

        except Exception:
            logging.exception("pnut_post")
            return "* something went wrong"

    def pnut_reply(api, postid, text, style):
        try:
            pnut_user, meta = api.get_user('me')
            replyto_msg, meta = api.get_post(postid)

            mentions = []
            if 'content' in replyto_msg:
                for m in replyto_msg['content']['entities']['mentions']:
                    if m['text'] != pnut_user['username']:
                        mentions.append("@" + m['text'] + ' ')

            rcpt = "@" + replyto_msg['user']['username'] + ' '
            cc = " ".join(mentions)
            if len(mentions) < 1 and style == "highlight":
                style = "single"

            if style == "single":
                reply = f"{rcpt}{text}"
            elif style == "all":
                reply = f"{rcpt}{cc}{text}"
            elif style == "global":
                reply = text
            elif style == "highlight":
                reply = f"{rcpt}{text} /{cc}"
            else:
                reply = text

            post, meta = api.create_post(data={'reply_to': postid, 'text': reply})
            return f"* created post {post['id']}"

        except Exception:
            logging.exception("pnut_reply")
            return "* something went wrong"


    def not_connected(message):
        reply = f"NuttyBot is not authorized with your pnut.io account.\n\n"
        reply += f"Visit the following url and then use the /connect command to save your auth code.\n\n"
        reply += f"{pnut_auth_url}?client_id={config['pnut']['client_id']}&redirect_uri={pnut_redirect}&scope={pnut_scopes}&response_type=token"

        bot.send_message(message.chat.id, reply)

    @bot.message_handler(commands=['help'])
    def handle_help(message):
        if message.chat.type != "private":
            return
        reply = "The following commands are available: \n\n"
        for key in bot_commands:
            reply += "/" + key + "   :  "
            reply += bot_commands[key] + "\n"
        bot.send_message(message.chat.id, reply)

    @bot.message_handler(commands=['start'])
    def handle_start(message):
        if message.chat.type != "private":
            return
        reply = "Welcome to NuttyBot.\n\n"

        pnut_api = pnut_user_api(message.chat.id)
        if pnut_api is not None:

            try:
                pnut_user, meta = pnut_api.get_user('me')
                reply += f"You are currently connected with pnut.io as {pnut_user.username}.\n\n"
                reply += "Simply send any message or image you would like to post or type "
                reply += "/help for a list of supported commands."

            except pnutpy.errors.PnutAuthAPIException:
                not_connected(message)
                return

            except Exception:
                logging.exception("handle_start")
                reply = "* something went wrong"

        else:
            not_connected(message)
            return

        bot.send_message(message.chat.id, reply)

    @bot.message_handler(commands=['connect'])
    def handle_connect(message):
        if message.chat.type != "private":
            return
        args = message.text.split(' ', maxsplit=1)

        if len(args) == 2:
            pnut_api = pnutpy.API.build_api(access_token=args[1])
            try:
                pnut_user, meta = pnut_api.get_user('me')

                try:
                    user = User.get(User.pnut_uid == pnut_user.id)
                    user.tg_enabled = True
                    user.tg_user = message.chat.id
                    user.pnut_token = args[1]
                    user.save()

                except User.DoesNotExist:
                    user = User(pnut_uid=pnut_user.id)
                    user.tg_enabled = True
                    user.tg_user = message.chat.id
                    user.pnut_token = args[1]
                    user.save()

                reply = "You have connected NuttyBot to your pnut.io account."

            except pnutpy.errors.PnutAuthAPIException:
                reply = "Unable verify access to your pnut.io account."

            except Exception:
                logging.exception("handle_connect")
                reply = "* something went wrong"

        else:

            reply = "* insufficient number of arguments\n\n"
            reply += "Try `/connect <token>`\n" 

        bot.send_message(message.chat.id, reply)

    @bot.message_handler(commands=['disconnect'])
    def handle_disconnect(message):
        if message.chat.type != "private":
            return
        try:
            user = User.get(User.tg_user == message.chat.id)
            user.tg_enabled = False
            user.tg_user = ""
            user.pnut_token = ""
            user.save()

            reply = "You have disconnected NuttyBot from your pnut.io account."

        except User.DoesNotExist:
            reply = "NuttyBot is not connected to your pnut.io account."

        except Exception:
            logging.exception("handle_disconnect")
            reply = "* something went wrong"

        bot.send_message(message.chat.id, reply)

    @bot.message_handler(commands=['enable'])
    def handle_enable(message):
        if message.chat.type != "private":
            return
        pnut_api = pnut_user_api(message.chat.id)
        if pnut_api is None:
            reply = "NuttyBot is not connected to your pnut.io account."
        else:
            try:
                pnut_user, meta = pnut_api.get_user('me')
                user = User.get(User.tg_user == message.chat.id)
                user.tg_enabled = True
                user.save()

                reply = "You have enabled notifications in NuttyBot."

            except pnutpy.errors.PnutAuthAPIException:
                reply = "NuttyBot is not connected to your pnut.io account."

            except User.DoesNotExist:
                reply = "NuttyBot is not connected to your pnut.io account."

            except Exception:
                logging.exception("handle_enable")
                reply = "* something went wrong"

        bot.send_message(message.chat.id, reply)

    @bot.message_handler(commands=['disable'])
    def handle_disable(message):
        if message.chat.type != "private":
            return
        try:
            user = User.get(User.tg_user == message.chat.id)
            user.tg_enabled = False
            user.save()

            reply = "You have disabled notifications in NuttyBot."

        except User.DoesNotExist:
            reply = "NuttyBot is not connected to your pnut.io account."

        except Exception:
            logging.exception("handle_disable")
            reply = "* something went wrong"

        bot.send_message(message.chat.id, reply)

    @bot.message_handler(commands=['copy_on'])
    def handle_copy_on(message):
        if message.chat.type != "private":
            return
        try:
            user = User.get(User.tg_user == message.chat.id)
            user.cc_notify = True
            user.save()

            reply = "Notify on copy has been enabled."

        except User.DoesNotExist:
            reply = "NuttyBot is not connected to your pnut.io account."

        except Exception:
            logging.exception("handle_copy_on")
            reply = "* something went wrong"

        bot.send_message(message.chat.id, reply)

    @bot.message_handler(commands=['copy_off'])
    def handle_copy_off(message):
        if message.chat.type != "private":
            return
        try:
            user = User.get(User.tg_user == message.chat.id)
            user.cc_notify = False
            user.save()

            reply = "Notify on copy has been disabled."

        except User.DoesNotExist:
            reply = "NuttyBot is not connected to your pnut.io account."

        except Exception:
            logging.exception("handle_copy_off")
            reply = "* something went wrong"

        bot.send_message(message.chat.id, reply)

    @bot.message_handler(commands=['pm_on'])
    def handle_pm_on(message):
        if message.chat.type != "private":
            return
        try:
            user = User.get(User.tg_user == message.chat.id)
            user.pm_notify = True
            user.save()

            reply = "Notify on pm has been enabled."

        except User.DoesNotExist:
            reply = "NuttyBot is not connected to your pnut.io account."

        except Exception:
            logging.exception("handle_copy_on")
            reply = "* something went wrong"

        bot.send_message(message.chat.id, reply)

    @bot.message_handler(commands=['pm_off'])
    def handle_pm_off(message):
        if message.chat.type != "private":
            return
        try:
            user = User.get(User.tg_user == message.chat.id)
            user.pm_notify = False
            user.save()

            reply = "Notify on pm has been disabled."

        except User.DoesNotExist:
            reply = "NuttyBot is not connected to your pnut.io account."

        except Exception:
            logging.exception("handle_copy_off")
            reply = "* something went wrong"

        bot.send_message(message.chat.id, reply)

    @bot.message_handler(commands=['settings'])
    def handle_settings(message):
        if message.chat.type != "private":
            return
        pnut_api = pnut_user_api(message.chat.id)
        try:
            if pnut_api is None:
                pnut_auth = "disconnected"
            else:
                pnut_user, meta = pnut_api.get_user('me')
                pnut_auth = "connected"

            user = User.get(User.tg_user == message.chat.id)
            tg_enabled = "enabled" if user.tg_enabled else "disabled"

            reply = ""
            reply += f"pnut.io account {pnut_auth}\n"
            reply += f"Telegram notifications {tg_enabled}\n"
            reply += f"Notify on copy mention (/@username) = {user.cc_notify}\n"
            reply += f"Notify on private messages (pm) = {user.pm_notify}\n"

        except User.DoesNotExist:
            reply = "NuttyBot is not connected to your pnut.io account."

        except Exception:
            logging.exception("handle_settings")
            reply = "* something went wrong"

        bot.send_message(message.chat.id, reply)

    @bot.message_handler(commands=['post'])
    def handle_post(message):
        if message.chat.type != "private":
            return
        pnut_api = pnut_user_api(message.chat.id)
        if pnut_api is None:
            reply = "* NuttyBot is not connected to your pnut.io account."
            bot.reply_to(message, reply)
            return

        args = message.text.split(' ', maxsplit=1)

        if len(args) == 2:
            reply = pnut_post(pnut_api, args[1])

        else:
            reply = "* insufficient number of arguments\n\n"
            reply += "Try `/post <text>`\n" 

        bot.send_message(message.chat.id, reply)

    @bot.message_handler(commands=['reply','re','replyall','ra','replyg','rg','replyh','rh'])
    def handle_reply(message):
        if message.chat.type != "private":
            return
        pnut_api = pnut_user_api(message.chat.id)
        if pnut_api is None:
            reply = "* NuttyBot is not connected to your pnut.io account."
            bot.reply_to(message, reply)
            return

        args = message.text.split(' ', maxsplit=2)

        if len(args) == 3:
            postid = args[1]
            text = args[2]

            if args[0] in ['/reply','/re']:
                style = "single"
            elif args[0] in ['/replyall','/ra']:
                style = "all"
            elif args[0] in ['/replyg','/rg']:
                style = "global"
            elif args[0] in ['/replyh','/rh']:
                style = "highlight"
            else:
                style = ""

            reply = pnut_reply(pnut_api, postid, text, style)

        else:
            reply = "* insufficient number of arguments\n\n"
            reply += "Try `/reply <postid> <text>`\n" 

        bot.send_message(message.chat.id, reply)

    @bot.message_handler(commands=['ping'])
    def handle_ping(message):
        if message.chat.type != "private":
            return
        bot.send_message(message.chat.id, "pong")

    @bot.message_handler(func=lambda m: True)
    def handle_message(message):
        if message.chat.type != "private":
            return

        pnut_api = pnut_user_api(message.chat.id)
        if pnut_api is None:
            reply = "* NuttyBot is not connected to your pnut.io account."
            bot.reply_to(message, reply)
            return

        if message.reply_to_message is not None:
            mparse = message.reply_to_message.text
            postid = re.search('posts\.pnut\.io\/([0-9]+)$',mparse).group(1)
            reply = pnut_reply(pnut_api, postid, message.text, "all")
        else:
            reply = pnut_post(pnut_api, message.text)

        bot.send_message(message.chat.id, reply)



    t = threading.Thread(target=mqueue, args=(bot,))
    t.start()

    bot.infinity_polling()
    _shutdown.set()

if __name__ == "__main__":
    main()
