import logging
import slixmpp
import pnutpy
import textwrap
import time
import redis
import threading
import json
import os

from argparse import ArgumentParser
from configobj import ConfigObj
from nuttybot.models import *

_shutdown = threading.Event()

REDIS_HOST = os.environ.get('REDIS_HOST', 'localhost')
REDIS_PORT = os.environ.get('REDIS_PORT', 6379)
NUTTYBOT_CONFIG = os.environ.get("NUTTYBOT_CONFIG")
NUTTYBOT_STORE = os.environ.get("NUTTYBOT_STORE")

REPLY_ONE = 1
REPLY_ALL = 2
REPLY_CC = 3

class XMPPBot(slixmpp.ClientXMPP):
    """
    The glorious NuttyBot traveling the tubes XMPP style.
    """

    def __init__(self, jid, password, pnut_clientid):
        slixmpp.ClientXMPP.__init__(self, jid, password)

        self.bot_commands = {
            '!auth': "Authenticate NuttyBot with pnut.io",
            '!connect {token}': "Store auth token authorizing NuttyBot",
            '!status': "Display your NuttyBot notification status",
            '!enable': "Enable NuttyBot notifications",
            '!disable': "Disable NuttyBot notifications",
            '!pm-enable': "Enable PM notifications",
            '!pm-disable': "Disable PM notifications",
            '!ping': "Check if bot is alive",
            '!reply {post_id} {text}': "Reply to author of post notification",
            '!replyg {post_id} {text}': "Reply globally to post notification",
            '!replyh {post_id} {text}': "Reply with cc to post notification",
            '!replyall {post_id} {text}': "Reply all to post notification",
            '!help': "Display this help text"
        }

        self.pnut_clientid = pnut_clientid
        self.pnut_auth_url = "https://pnut.io/oauth/authenticate"
        self.pnut_redirect = "urn:ietf:wg:oauth:2.0:oob"
        self.pnut_scopes = "basic,stream,write_post,follow,presence,messages,files"

        self.add_event_handler("session_start", self.start)
        self.add_event_handler("message", self.on_message)

    def start(self, event):
        self.send_presence()
        self.get_roster()

    def on_message(self, msg):
        if msg['type'] in ('chat', 'normal'):

            if msg['body'].startswith("!"):
                argv = msg['body'].split(' ', maxsplit=2)
                cmd = argv[0]

                if cmd.lower() == "!help":
                    self.cmd_help(msg)

                elif cmd.lower() == "!ping":
                    msg.reply("PONG").send()

                elif cmd.lower() == "!auth":
                    self.cmd_auth(msg)

                elif cmd.lower() == "!connect":
                    self.cmd_connect(msg, argv[1])

                elif cmd.lower() == "!status":
                    self.cmd_status(msg)

                elif cmd.lower() == "!enable":
                    self.cmd_enable(msg)

                elif cmd.lower() == "!pm-enable":
                    self.cmd_pm_enable(msg)

                elif cmd.lower() == "!pm-disable":
                    self.cmd_pm_disable(msg)

                elif cmd.lower() == "!disable":
                    self.cmd_disable(msg)

                elif cmd.lower() == "!reply":
                    if len(argv) != 3:
                        msg.reply("Not enough arguments\n\n").send()
                        self.cmd_help(msg)

                    reply_postid = argv[1]
                    reply_text = argv[2]
                    self.send_post(msg, reply_text, reply_postid, REPLY_ONE)

                elif cmd.lower() == "!replyg":
                    if len(argv) != 3:
                        msg.reply("Not enough arguments\n\n").send()
                        self.cmd_help(msg)

                    reply_postid = argv[1]
                    reply_text = argv[2]
                    self.send_post(msg, reply_text, reply_postid)

                elif cmd.lower() == "!replyh":
                    if len(argv) != 3:
                        msg.reply("Not enough arguments\n\n").send()
                        self.cmd_help(msg)

                    reply_postid = argv[1]
                    reply_text = argv[2]
                    self.send_post(msg, reply_text, reply_postid, REPLY_CC)

                elif cmd.lower() == "!replyall":
                    if len(argv) != 3:
                        msg.reply("Not enough arguments\n\n").send()
                        self.cmd_help(msg)

                    reply_postid = argv[1]
                    reply_text = argv[2]
                    self.send_post(msg, reply_text, reply_postid, REPLY_ALL)

                else:
                    msg.reply("UNKNOWN COMMAND").send()

            else:
                self.send_post(msg, msg['body'])

    def cmd_help(self, msg):
        reply = "THE FOLLOWING COMMANDS ARE AVAILABLE: \n\n"
        for key in self.bot_commands:
            reply += key + "  :  " 
            reply += self.bot_commands[key] + "\n"
        msg.reply(reply).send()

    def cmd_auth(self, msg):
        reply = "NuttyBot is not authorized with your pnut.io account. "
        reply += "Visit the following URL and then use the !connect command "
        reply += "to save your authentication token.\n\n"

        reply += f"{self.pnut_auth_url}?client_id={self.pnut_clientid}"
        reply += f"&redirect_uri={self.pnut_redirect}&scope={self.pnut_scopes}"
        reply += f"&response_type=token"
        msg.reply(reply).send()

    def cmd_connect(self, msg, token):
        pnut_api = pnutpy.API.build_api(access_token=token)

        try:
            pnut_user, meta = pnut_api.get_user('me')

            try:
                user = User.get(User.pnut_uid == pnut_user.id)
                user.xmpp_enabled = True
                user.xmpp_user = msg['from'].bare
                user.pnut_token = token
                user.save()

            except User.DoesNotExist:
                user = User(pnut_uid=pnut_user.id)
                user.xmpp_enabled = True
                user.xmpp_user = msg['from'].bare
                user.pnut_token = token
                user.save()

            reply = "You have connected NuttyBot to your pnut.io account."

        except pnutpy.errors.PnutAuthAPIException:
            reply = "Unable verify access to your pnut.io account."

        except Exception:
            logging.exception("xmpp_handle_connect")
            reply = "ERROR, SOMETHING WENT WRONG"

        msg.reply(reply).send()

    def cmd_status(self, msg):
        user = self.get_nuttybot_user(msg['from'].bare)

        if user is not None:
            pnut_api = pnutpy.API.build_api(access_token=user.pnut_token)
            try:
                pnut_user, meta = pnut_api.get_user('me')
                reply = f"Connected to pnut.io as {pnut_user.username}\n"
                enabled = "enabled" if user.xmpp_enabled else "disabled"
                reply += f"Notifications are {enabled}\n"
                pm_status = "enabled" if user.pm_notify else "disabled"
                reply += f"PM notifications are {pm_status}"

            except pnutpy.errors.PnutAuthAPIException:
                reply = "Not authorized with pnut.io"

        else:
            reply = "Not authorized with pnut.io"

        msg.reply(reply).send()

    def cmd_enable(self, msg):
        user = self.get_nuttybot_user(msg['from'].bare)

        if user is not None:
            pnut_api = pnutpy.API.build_api(access_token=user.pnut_token)
            try:
                pnut_user, meta = pnut_api.get_user('me')
                user.xmpp_enabled = True
                user.save()

                reply = "Notifications enabled"

            except pnutpy.errors.PnutAuthAPIException:
                reply = "Not authorized with pnut.io"

        else:
            reply = "Not authorized with pnut.io"

        msg.reply(reply).send()

    def cmd_disable(self, msg):
        user = self.get_nuttybot_user(msg['from'].bare)

        if user is not None:
            pnut_api = pnutpy.API.build_api(access_token=user.pnut_token)
            try:
                pnut_user, meta = pnut_api.get_user('me')
                user.xmpp_enabled = False
                user.save()

                reply = "Notifications disabled"

            except pnutpy.errors.PnutAuthAPIException:
                reply = "Not authorized with pnut.io"

        else:
            reply = "Not authorized with pnut.io"

        msg.reply(reply).send()

    def cmd_pm_enable(self, msg):
        user = self.get_nuttybot_user(msg['from'].bare)

        if user is not None:
            pnut_api = pnutpy.API.build_api(access_token=user.pnut_token)
            try:
                pnut_user, meta = pnut_api.get_user('me')
                user.pm_notify = True
                user.save()

                reply = "PM notifications enabled"

            except pnutpy.errors.PnutAuthAPIException:
                reply = "Not authorized with pnut.io"

        else:
            reply = "Not authorized with pnut.io"

        msg.reply(reply).send()

    def cmd_pm_disable(self, msg):
        user = self.get_nuttybot_user(msg['from'].bare)

        if user is not None:
            pnut_api = pnutpy.API.build_api(access_token=user.pnut_token)
            try:
                pnut_user, meta = pnut_api.get_user('me')
                user.pm_notify = False
                user.save()

                reply = "PM notifications disabled"

            except pnutpy.errors.PnutAuthAPIException:
                reply = "Not authorized with pnut.io"

        else:
            reply = "Not authorized with pnut.io"

        msg.reply(reply).send()

    def get_nuttybot_user(self, jid):
        try:
            return User.get(User.xmpp_user == jid)

        except User.DoesNotExist:
            return None

    def send_post(self, msg, text, reply_to=None, mention=None):
        user = self.get_nuttybot_user(msg['from'].bare)

        if user is not None and len(user.pnut_token) > 0:
            pnut_api = pnutpy.API.build_api(access_token=user.pnut_token)

            try:
                if mention is not None:
                    pnut_user, meta = pnut_api.get_user("me")
                    me = pnut_user["username"]
                    orig_msg, meta = pnut_api.get_post(reply_to)
                    author = orig_msg["user"]["username"]
                    # text = "@" + author + " " + text
                    highlight = "@" + author

                    if mention == REPLY_ONE:
                        text = highlight + " " + text

                    elif mention in [REPLY_ALL, REPLY_CC]:
                        cc_list = []
                        if "content" in orig_msg:
                            if "mentions" in orig_msg["content"]["entities"]:
                                mentions = orig_msg["content"]["entities"]["mentions"]
                            else:
                                mentions = []

                            for m in mentions:
                                if m["text"] != me and m["text"] != author:
                                    cc_list.append("@" + m["text"])
                        if len(cc_list) > 0:
                            cc = " ".join(cc_list)

                            if mention == REPLY_CC:
                                text = highlight + " " + text + " /" + cc
                            elif mention == REPLY_ALL:
                                text = highlight + " " + cc + " " + text

                # check the length of the post and if it's too long convert it
                raw = []
                lcheck = textwrap.wrap(text, 256)
                if len(lcheck) > 1:
                    pretext = textwrap.wrap(text, 250)
                    longpost = {
                        'type': "nl.chimpnut.blog.post",
                        'value': {
                            'body': text,
                            'tstamp': time.time() * 1000
                        }
                    }
                    raw.append(longpost)
                    text = pretext + " [...](" + entry.link + ")"

                post, meta = pnut_api.create_post(
                    data={'reply_to': reply_to, 'text': text, 'raw': raw})
                reply = f"* created post {post['id']}"

            except pnutpy.errors.PnutAuthAPIException:
                reply = "Not authorized with pnut.io"

            except Exception:
                logging.exception("send_post")
                reply = f"BAD"

        else:
            reply = "UNKNOWN"

        msg.reply(reply).send()

def notify_queue(bot):
    rs = redis.Redis(host=REDIS_HOST, port=REDIS_PORT)

    while not _shutdown.isSet():
        try:
            item = rs.lpop('XMPP')
            if item is not None:
                msg = json.loads(item)
                logging.debug(msg)
                bot.send_message(mto=msg['user'], mbody=msg['message'],
                    mtype='chat')
            time.sleep(2)

        except redis.exceptions.ConnectionError:
            logging.warning("--- redis connection error ---")
            logging.warning("sleeping for 30...")
            time.sleep(30)

        except Exception:
            logging.exception("notify_queue")
            logging.error("--- unknown redis error ---")
            logging.warning("sleeping for 30...")
            time.sleep(30)

    logging.info("exiting notify_queue")

def main():
    parser = ArgumentParser(description=XMPPBot.__doc__)
    parser.add_argument("-q", "--quiet", help="set logging to ERROR",
        action="store_const", dest="loglevel",
        const=logging.ERROR, default=logging.INFO)
    parser.add_argument("-d", "--debug", help="set logging to DEBUG",
        action="store_const", dest="loglevel",
        const=logging.DEBUG, default=logging.INFO)
    parser.add_argument("-c", "--config", help="path to config file",
        action="store", dest="configfile", default="config.ini")
    parser.add_argument("-s", "--store", help="path to database file",
        action="store", dest="dbfile", default="store.db")

    args = parser.parse_args()

    logging.basicConfig(level=args.loglevel)

    if NUTTYBOT_CONFIG is None:
        config = ConfigObj(args.configfile)
    else:
        config = ConfigObj(NUTTYBOT_CONFIG)

    if "xmpp" not in config:
        logging.warning("xmpp section missing from configuration, exiting")
        exit()
    elif "jid" not in config["xmpp"]:
        logging.error("xmpp jid missing from configuration, exiting")
        exit(1)
    elif "password" not in config["xmpp"]:
        logging.error("xmpp password missing from configuration, exiting")
        exit(1)

    jid = config["xmpp"]["jid"]
    password = config["xmpp"]["password"]

    if "pnut" not in config:
        logging.error("pnut section missing from configuration")
        exit(1)
    elif "client_id" not in config["pnut"]:
        logging.error("pnut client_id missing from configuration")
        exit(1)

    client_id = config["pnut"]["client_id"]

    if NUTTYBOT_STORE is None:
        db.init(args.dbfile)
    else:
        db.init(NUTTYBOT_STORE)
    create_tables()

    xmpp = XMPPBot(jid, password, client_id)
    xmpp.register_plugin('xep_0030') # Service Discovery
    xmpp.register_plugin('xep_0004') # Data Forms
    xmpp.register_plugin('xep_0060') # PubSub
    xmpp.register_plugin('xep_0199') # XMPP Ping

    t = threading.Thread(target=notify_queue, args=(xmpp,))
    t.start()

    xmpp.connect()
    xmpp.process()

    _shutdown.set()

if __name__ == '__main__':
    main()
