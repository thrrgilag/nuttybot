from peewee import *
from playhouse.migrate import *

import logging

db = SqliteDatabase(None)
migrator = SqliteMigrator(db)

db_schema = "3"

class BaseModel(Model):
    class Meta:
        database = db

class User(BaseModel):
    pnut_uid = CharField(unique=True)
    pnut_token = CharField(null=True)
    po_enabled = BooleanField(null=True)
    po_user = CharField(null=True)
    tg_enabled = BooleanField(null=True)
    tg_user = CharField(null=True)
    cc_notify = BooleanField(default=True)
    pm_notify = BooleanField(default=True) # added to schema v2
    xmpp_enabled = BooleanField(null=True) # added to schema v3
    xmpp_user = CharField(null=True) #added to schema v3

class System(BaseModel):
    key = CharField(unique=True)
    value = CharField()

def create_tables():
    with db:
        if not db.table_exists(User._meta.table_name) or not db.table_exists(System._meta.table_name):
            logging.debug("...creating tables...")
            db.create_tables([User, System])
            System.replace(key="schema", value=db_schema).execute()

        else:
            logging.debug("...skipping table creation...")
            try:
                sver = System.get(key="schema")
                if sver.value == db_schema:
                    logging.debug("...schema looks fine...")
                elif sver.value == "2":
                    logging.debug("...updating schema from v2...")
                    migrate_tables_v2()
                else:
                    logging.debug(f"!!! I don't have a migration for schema {sver.value} !!!")

            except System.DoesNotExist:
                logging.debug("...no schema version...")
                logging.debug("...updating schema...")
                migrate_tables_all()

# see https://docs.peewee-orm.com/en/latest/peewee/playhouse.html#schema-migrations
def migrate_tables_all():
    pm_notify = BooleanField(default=True)
    migrate(
        migrator.add_column(User._meta.table_name, 'pm_notify', pm_notify),
    )
    # add addtional migrations when created
    migrate_tables_v2()

def migrate_tables_v2():
    xmpp_enabled = BooleanField(null=True)
    xmpp_user = CharField(null=True)
    migrate(
        migrator.add_column(User._meta.table_name, 'xmpp_enabled', xmpp_enabled),
        migrator.add_column(User._meta.table_name, 'xmpp_user', xmpp_user),
    )
    migrate_tables_v3()

def migrate_tables_v3():
    # TODO: add migrations for v4 when needed
    System.replace(key="schema", value=db_schema).execute()
