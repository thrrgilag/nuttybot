import pnutpy
import logging
import websocket
import time
import json
import _thread
import argparse
import requests
import redis
import os

from configobj import ConfigObj
from nuttybot.models import *

REDIS_HOST = os.environ.get('REDIS_HOST', 'localhost')
REDIS_PORT = os.environ.get('REDIS_PORT', 6379)
NUTTYBOT_CONFIG = os.environ.get("NUTTYBOT_CONFIG")
NUTTYBOT_STORE = os.environ.get("NUTTYBOT_STORE")

rs = redis.Redis(host=REDIS_HOST, port=REDIS_PORT)

class PnutBot:

    def __init__(self, config, db):
        self.config = config
        self.db = db
        self.api = pnutpy.api
        self.api.add_authorization_token(self.config["pnut"]["bot_token"])
        websocket.enableTrace(False)
        self.ws = websocket.WebSocketApp(self.config["pnut"]["ws_url"],
                              on_message = self.on_message,
                              on_error = self.on_error,
                              on_close = self.on_close)
        self.ws.on_open = self.on_open

    def on_error(self, ws, error):
        logging.error('*** error on connection with pnut.io ***')
        logging.error(error)

    def on_close(self, ws, status_code, msg):
        logging.info("--- closing connection with pnut.io ---")
        logging.debug(status_code)
        logging.debug(msg)

    def on_open(self, ws):
        logging.info("--- starting connection with pnut.io ---")

        def run(*args):
            while True:
                ws.send(".")
                if "monitor" in self.config and "monitor_url" in self.config["monitor"]:
                    r = requests.get(self.config["monitor"]["monitor_url"])
                time.sleep(30)
            logging.info("--- terminating ---")

        try:
            s = System.get(key="lastpost")
            logging.info(f"--- fetching posts since {s.value}")
            posts, meta = self.api.posts_streams_global(since_id=s.value)
            self.handle_post(posts, meta)

        except System.DoesNotExist:
            pass

        except Exception:
            logging.exception("fetch global")

        _thread.start_new_thread(run, ())

    def on_message(self, ws, message):
        msg = json.loads(message)
        if 'meta' in msg:
            meta = msg['meta']
            logging.debug(meta.keys())
        else:
            logging.debug('*** unknown server message ***')
            logging.debug(message)
            return

        if 'is_deleted' in meta:
            logging.debug("* deleted item")
            return

        if 'type' in meta:
            logging.debug(f"type: {meta['type']}")
            if meta['type'] == "post" and 'data' in msg:
                self.handle_post(msg['data'], meta)

            elif meta['type'] == "message":
                self.handle_message(msg['data'], meta)

    def handle_message(self, data, meta):
        logging.debug("* handle message")
        if 'suppress_notifications' in meta:
            logging.debug(f"suppress {meta['suppress_notifications']}")
        logging.debug(meta)

        if meta['channel_type'] == 'io.pnut.core.chat':
            pass

        elif meta['channel_type'] == 'io.pnut.core.pm':
            logging.debug("--pm--")
            if str(self.config["pnut"]["bot_id"]) in meta['subscribed_user_ids']:
                logging.debug("BOT COMMAND!")
                self.bot_command(data)
            else:
                self.handle_pm(data, meta)

    def handle_post(self, data, meta):
        logging.debug("* handle post")
        suppress = []
        if 'suppress_notifications' in meta:
            logging.debug(f"suppress {meta['suppress_notifications']}")
            suppress = meta['suppress_notifications']

        for post in data:

            if 'repost_of' in post:
                logging.debug(f"repost of {post['repost_of']['id']}, skipping notification")
                continue

            username = post['user']['username']
            post_url = f"https://posts.pnut.io/{post['id']}"
            if 'content' in post:
                if 'entities' in post['content']:
                    if 'mentions' in post['content']['entities']:
                        for mention in post['content']['entities']['mentions']:
                            if 'is_copy' in mention:
                                is_copy = True
                            else:
                                is_copy = False

                            notice = f"{username} mentioned you in a post.\n\n"
                            notice += post['content']['text'] + "\n\n"
                            notice += f"{post['id']} - {post_url}\n"

                            if mention['id'] not in suppress:
                                self.notify(mention['id'], notice, post_url, is_copy=is_copy)

                    logging.debug(post['content']['entities'])

        if 'id' in meta:
            c = (System.replace(key="lastpost", value=meta['id']).execute())

    def handle_pm(self, data, meta):
        logging.debug("* handle pm")
        if 'suppress_notifications' in meta:
            logging.debug(f"suppress {meta['suppress_notifications']}")

        for pm in data:
            pm_url = f"https://beta.pnut.io/channels/{pm['channel_id']}"
            from_id = pm['user']['id']
            from_user = pm['user']['username']
            notify_ids = meta['subscribed_user_ids']
            notify_ids.remove(str(from_id))

            for s in meta['suppress_notifications']:
                notify_ids.remove(s)

            notice = f"You have new messages from {from_user}.\n\n"
            notice += f"{pm_url}\n"

            for nid in notify_ids:
                self.notify(nid, notice, pm_url, is_pm=True)

    def notify(self, user_id, msg, url, **kwargs):
        is_pm = kwargs.get('is_pm', False)
        is_copy = kwargs.get('is_copy', False)
        send_notice = True

        try:
            user = User.get(User.pnut_uid == user_id)

            if is_pm:
                if not user.pm_notify:
                    send_notice = False

            if is_copy:
                if not user.cc_notify:
                    send_notice = False

            if user.po_enabled and len(user.po_user) > 0 and send_notice:
                logging.info(f"- sending pushover notification to {user_id}")
                logging.debug(msg)
                self.pushover(user.po_user, msg, url)

            if user.tg_enabled and len(user.tg_user) > 0 and send_notice:
                logging.info(f"- sending telegram notification to {user_id}")
                logging.debug(msg)
                self.telegram(user.tg_user, msg)

            if user.xmpp_enabled and len(user.xmpp_user) > 0 and send_notice:
                logging.info(f"- sending xmpp notification to {user_id}")
                logging.debug(msg)
                self.xmpp(user.xmpp_user, msg)

        except User.DoesNotExist:
            return

        except Exception:
            logging.exception("notify")

    def bot_command(self, data):

        for msg in data:
            if msg['user']['id'] == str(self.config["pnut"]["bot_id"]):
                continue

            argv = msg['content']['text'].split(' ', 1)
            action = argv[0]
            args = argv[1] if len(argv) > 1 else ""
            logging.debug(action)
            logging.debug(args)

            if action.lower() == 'help':
                self.cmd_help(msg['channel_id'])

            elif action.lower() == 'ping':
                self.cmd_ping(msg['channel_id'])

            elif action.lower() == 'pushover_on':
                self.cmd_pushover_on(msg['channel_id'], msg['user']['id'], args)

            elif action.lower() == 'pushover_off':
                self.cmd_pushover_off(msg['channel_id'], msg['user']['id'])

            elif action.lower() == 'settings':
                self.cmd_settings(msg['channel_id'], msg['user']['id'])

            elif action.lower() == 'copy_on':
                self.cmd_copy_on(msg['channel_id'], msg['user']['id'])

            elif action.lower() == 'copy_off':
                self.cmd_copy_off(msg['channel_id'], msg['user']['id'])

            elif action.lower() == 'pm_on':
                self.cmd_pm_on(msg['channel_id'], msg['user']['id'])

            elif action.lower() == 'pm_off':
                self.cmd_pm_off(msg['channel_id'], msg['user']['id'])

            else:
                reply = f"I do not know this word, {action}\n"
                self.reply(msg['channel_id'], reply)

    def cmd_help(self, channel_id):
        title = "The following commands can be given directly in this chat\n\n"

        commands = ""
        commands += "settings  : Display your current notification settings\n"
        commands += "copy_on  : Enable notify on copy mention (/@username)\n"
        commands += "copy_off  : Disable notify on copy mention (/@username)\n"
        commands += "pm_on  : Enable private message (pm) notifications\n"
        commands += "pm_off  : Disable private message (pm) notifications\n"
        commands += "pushover_on <user key>  : Add user key to enable Pushover notifications\n"
        commands += "pushover_off  : Remove user key to disable Pushover notifications\n"
        commands += "ping  : check if bot is alive\n"
        commands += "help  : display this help message\n" 

        reply = title + commands + "\n"
        self.reply(channel_id, reply)

    def cmd_settings(self, channel_id, user_id):
        try:
            user = User.get(User.pnut_uid == user_id)
            po_enabled = "enabled" if user.po_enabled else "disabled"
            tg_enabled = "enabled" if user.tg_enabled else "disabled"
            if user.pnut_token is not None and user.tg_user is not None:
                if len(user.pnut_token) > 0 and len(user.tg_user) > 0:
                    tg_auth = user.tg_user
                else:
                    tg_auth = None
            else:
                tg_auth = None

            reply = ""
            reply += f"Pushover notifications {po_enabled}\n"
            reply += f"Pushover user key = {user.po_user}\n"
            reply += f"Telegram notifications {tg_enabled}\n"
            reply += f"Telegarm account {tg_auth}\n"
            reply += f"Notify on copy mention (/@username) = {user.cc_notify}\n"
            reply += f"Notify on private messages (pm) = {user.pm_notify}\n"

        except User.DoesNotExist:
            reply = f"You are not currently setup for notifications in NuttyBot\n"

        except Exception:
            logging.exception("cmd_settings")
            reply = f"Error, something went wrong.\n"

        self.reply(channel_id, reply)

    def cmd_ping(self, channel_id):
        logging.info("- ping, pong -")
        self.reply(channel_id, "pong")

    def cmd_pushover_on(self, channel_id, pnut_user, args):
        if len(args) < 1:
            reply = f"Error, insufficient arguments.\n\n"
            self.cmd_help()
            return

        try:
            user = User.get(pnut_uid=pnut_user)
            user.po_enabled = True
            user.po_user = args
            user.save()
            reply = f"Pushover notifications have been enabled.\n"

        except User.DoesNotExist:
            user = User(pnut_uid=pnut_user)
            user.po_enabled = True
            user.po_user = args
            user.save()
            reply = f"Pushover notifications have been enabled.\n"

        except Exception:
            logging.exception("cmd_pushover_on")
            reply = f"Error, something went wrong.\n"

        self.reply(channel_id, reply)

    def cmd_pushover_off(self, channel_id, pnut_user):
        try:
            user = User.get(pnut_uid=pnut_user)
            user.po_enabled = False
            user.po_user = ""
            user.save()
            reply = f"Pushover notifications have been disabled.\n"

        except User.DoesNotExist:
            user = User(pnut_uid=pnut_user)
            user.po_enabled = False
            user.po_user = ""
            user.save()
            reply = f"Pushover notifications have been disabled.\n"

        except Exception:
            logging.exception("cmd_pushover_off")
            reply = f"Error, something went wrong.\n"

        self.reply(channel_id, reply)

    def cmd_copy_on(self, channel_id, pnut_user):
        try:
            user = User.get(pnut_uid=pnut_user)
            user.cc_notify = True
            user.save()
            reply = f"Notify on copy has been enabled.\n"
            
        except User.DoesNotExist:
            user = User(pnut_uid=pnut_user)
            user.cc_notify = True
            user.save()
            reply = f"Notify on copy has been enabled.\n"

        except Exception:
            logging.exception("cmd_copy_on")
            reply = f"Error, something went wrong.\n"

        self.reply(channel_id, reply)

    def cmd_copy_off(self, channel_id, pnut_user):
        try:
            user = User.get(pnut_uid=pnut_user)
            user.cc_notify = False
            user.save()
            reply = f"Notify on copy has been disabled.\n"
            
        except User.DoesNotExist:
            user = User(pnut_uid=pnut_user)
            user.cc_notify = False
            user.save()
            reply = f"Notify on copy has been disabled.\n"

        except Exception:
            logging.exception("cmd_copy_off")
            reply = f"Error, something went wrong.\n"

        self.reply(channel_id, reply)

    def cmd_pm_on(self, channel_id, pnut_user):
        try:
            user = User.get(pnut_uid=pnut_user)
            user.pm_notify = True
            user.save()
            reply = f"Notify on pm has been enabled.\n"

        except User.DoesNotExist:
            user = User(pnut_uid=pnut_user)
            user.pm_notify = True
            user.save()
            reply = f"Notify on pm has been enabled.\n"

        except Exception:
            logging.exception("cmd_pm_on")
            reply = f"Error, something went wrong.\n"

        self.reply(channel_id, reply)

    def cmd_pm_off(self, channel_id, pnut_user):
        try:
            user = User.get(pnut_uid=pnut_user)
            user.pm_notify = False
            user.save()
            reply = f"Notify on pm has been disabled.\n"

        except User.DoesNotExist:
            user = User(pnut_uid=pnut_user)
            user.pm_notify = False
            user.save()
            reply = f"Notify on pm has been disabled.\n"

        except Exception:
            logging.exception("cmd_pm_off")
            reply = f"Error, something went wrong.\n"

        self.reply(channel_id, reply)

    def reply(self, channel_id, text):
        try:
            self.api.create_message(channel_id, data={'text': text})

        except Exception:
            logging.exception("reply")

    def pushover(self, user, text, link):
        url = 'https://api.pushover.net/1/messages.json'

        if "pushover" in self.config and "app_token" in self.config["pushover"]:
            params = {
                'token': self.config["pushover"]["app_token"],
                'user': user,
                'message': text,
                'url': link
            }
            resp = requests.post(url, data=params)

    def telegram(self, user, text):
        msg = {
            'user': user,
            'message': text
        }
        rs.rpush('TG', json.dumps(msg))

    def xmpp(self, user, text):
        msg = {
            'user': user,
            'message': text
        }
        rs.rpush('XMPP', json.dumps(msg))

def main():
    a_parser = argparse.ArgumentParser()
    a_parser.add_argument(
        '-d', action='store_true', dest='debug',
        help="debug logging"
    )
    a_parser.add_argument(
        '-c', '--config', default="config.ini",
        help="configuration file"
    )
    a_parser.add_argument(
        '-s', '--store', default="store.db",
        help="sqlite database filepath"
    )
    args = a_parser.parse_args()

    if args.debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    if NUTTYBOT_CONFIG is None:
        filename = args.config
    else:
        filename = NUTTYBOT_CONFIG

    if NUTTYBOT_STORE is None:
        db_file = args.store
    else:
        db_file = NUTTYBOT_STORE

    config = ConfigObj(filename)

    if "pnut" not in config:
        logging.error("pnut section missing from configuration")
        exit(1)
    elif "bot_id" not in config["pnut"]:
        logging.error("bot_id missing from pnut section in configuration")
        exit(1)
    elif "bot_token" not in config["pnut"]:
        logging.error("bot_token missing from pnut section in configuration")
        exit(1)
    elif "ws_url" not in config["pnut"]:
        logging.error("ws_url missing from pnut section in configuration")
        exit(1)
    elif "client_id" not in config["pnut"]:
        logging.error("client_id missing from pnut section in configuration")
        exit(1)

    db.init(db_file)
    create_tables()

    pnutbot = PnutBot(config, db)

    while True:
        running = pnutbot.ws.run_forever()
        if not running:
            break
        time.sleep(30)
    pnutbot.ws.close()

if __name__ == "__main__":
    main()
