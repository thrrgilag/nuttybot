FROM python:3.11-slim-bookworm AS builder

RUN pip install --no-cache-dir --upgrade pip setuptools wheel

WORKDIR /usr/src/app

COPY . .

RUN pip wheel . --wheel-dir /wheels --find-links /wheels



FROM python:3.11-slim-bookworm AS run

WORKDIR /app

COPY --from=builder /wheels /wheels
COPY entrypoint.sh .

RUN pip --no-cache-dir install --find-links /wheels --no-index nuttybot

RUN addgroup -gid 2222 nuttybot \
    && adduser --home /home/nuttybot --uid 2222 --gid 2222 --disabled-password nuttybot

USER nuttybot
VOLUME /data
WORKDIR /data
ENTRYPOINT [ "/app/entrypoint.sh" ]
