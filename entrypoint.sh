#!/bin/bash

# XMPP bot
nuttybot_xmpp $ARGS -c /data/config.ini -s /data/store.db &

# Telegram bot
nuttybot_tg $ARGS -c /data/config.ini -s /data/store.db &

# Pnut bot
nuttybot $ARGS -c /data/config.ini -s /data/store.db &

wait -n

exit $?
