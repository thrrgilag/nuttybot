# NuttyBot

A bot connecting pnut.io with Telegram, XMPP, and Pushover


## XMPP usage

To get started using XMPP, simply start a chat with [nuttybot@mcmillian.dev] and issue the `!auth` command. Follow the instructions to connect NuttyBot to your pnut.io account.

The following comands are currently supported.

```
!auth                      : Authenticate NuttyBot with pnut.io
!connect {token}            : Store auth token authorizing NuttyBot
!status                     : Display your NuttyBot notification status
!enable                     : Enable NuttyBot notifications
!disable                    : Disable NuttyBot notifications
!pm-enable                  : Enable PM notifications
!pm-disable                 : Disable PM notifications
!ping                       : Check if bot is alive
!reply {post_id} {text}     : Reply to author of post notification
!replyg {post_id} {text}    : Reply globally to post notification
!replyh {post_id} {text}'   : Reply with cc to post notification
!replyall {post_id} {text}  : Reply all to post notification
!help : Display this help text
```


## Telegram usage

To get started using Telegram, simply start a chat with [@pnutbot] and issue the `/start` command. Follow the instructions and link to authorize NuttyBot to your pnut.io account then your all set!

@ mentions will be delivered to you in Telegram and you can reply or post new messages using your Telegram client.

The following commands are currently supported.

```
/start                     :  Display the welcome messaage
/connect <token>           :  Store auth token provided after authorizing NuttyBot
/disconnect                :  Remove auth token and disable NuttyBot
/enable                    :  Enable Telegram notifications in NuttyBot
/disable                   :  Disable Telegram notifications in NuttyBot
/copy_on                   :  Enable notification on copy mention (/@username)
/copy_off                  :  Disable notification on copy mention (/@username)
/settings                  :  Display your NuttyBot notification settings
/post <text>               :  Post new message to your timeline
/reply <postid> <text>     :  Reply to the poster of postid
/replyall <postid> <text>  :  Reply to all mentioned in postid
/replyg <postid> <text>    :  Reply to postid with a global context (no @)
/replyh <postid> <text>    :  Reply to all mentioned but add others at end with a /
/ping                      :  Check if NuttyBot is alive
/help                      :  Display help text
```


## Pushover usage

To get started receiving notifications via Pushover, follow [@NuttyBot] on pnut.io and send a private message with the command `help`. Use the `pushover_on` command with your user key which can be found on pushover.net.

The following commands are currently supported.

```
settings                :  Display your current notification settings
copy_on                 :  Enable notify on copy mention (/@username)
copy_off                :  Disable notify on copy mention (/@username)
pushover_on <user key>  :  Add user key to enable Pushover notifications
pushover_off            :  Remove user key to disable Pushover notifications
ping                    :  check if bot is alive
help                    :  display this help message
```


## Contributing and support

You can open issues for bugs or feature requests and you can submit merge requests to this project on [GitLab]. You can also submit issues and patches directly to [morgan@mcmillian.dev].

Join my public chat room for development discussion.

- [dev room on pnut.io]
- [devel@conference.mcmillian.dev]


## License

AGPLv3, see [LICENSE].


[@pnutbot]: https://t.me/pnutbot
[@NuttyBot]: https://pnut.io/@nuttybot
[nuttybot@mcmillian.dev]: xmpp:nuttybot@mcmillian.dev
[GitLab]: https://gitlab.com/thrrgilag/nuttybot
[morgan@mcmillian.dev]: mailto:morgan@mcmillian.dev
[dev room on pnut.io]: https://patter.chat/85
[devel@conference.mcmillian.dev]: xmpp:devel@conference.mcmillian.dev?join
[LICENSE]: LICENSE
