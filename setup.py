from setuptools import setup

setup(
    name='nuttybot',
    version='2.2.0',
    description = 'A bot connecting pnut.io with XMPP, Telegram and Pushover',
    author = 'Morgan McMillian',
    author_email = 'morgan@mcmillian.dev',
    url = 'https://gitlab.com/thrrgilag/nuttybot',
    license = 'AGPL-3.0',
    py_modules=[
        'nuttybot.models',
        'nuttybot.pnutbot',
        'nuttybot.tbot',
        'nuttybot.xmppbot',
    ],
    install_requires=[
        'PyYAML',
        'pnutpy',
        'websocket-client',
        'peewee',
        'requests',
        'redis',
        'pyTelegramBotAPI',
        'slixmpp',
        'configobj',
    ],
    entry_points={
        'console_scripts': [
            'nuttybot = nuttybot.pnutbot:main',
            'nuttybot_tg = nuttybot.tbot:main',
            'nuttybot_xmpp = nuttybot.xmppbot:main',
        ]
    },
)

